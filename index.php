<?php
require_once "bitFlyerClient.php";
require_once "settings.php";

use musikirin\bitFlyerClient;
use musikirin\OptionData;
use musikirin\ChildOrder;
use musikirin\ParentOrder;
use musikirin\ParentOrderParameter;

// クライアントのインスタンス化
$client = new bitFlyerClient($market, $API_KEY,$API_SECRET);

?>

<!doctype html>
<html lang="jp">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php
    $pd = new OptionData(['count' => 2]);
    $co = new ChildOrder(
            'BTC_JPY',
            'LIMIT',
            'BUY',
            '900000',
            '0.01',
            '1',
            'GTC'
        );

    $pop1 = new ParentOrderParameter(
            'BTC_JPY',
            'LIMIT',
            'BUY',
            '900000',
            '',
            '0.01'
    );
    $pop2 = new ParentOrderParameter(
        'BTC_JPY',
        'LIMIT',
        'SELL',
        '1900000',
        '',
        '0.01'
    );
    $pop3 = new ParentOrderParameter(
        'BTC_JPY',
        'STOP_LIMIT',
        'SELL',
        '800000',
        '780000',
        '0.01'
    );

    $po = new ParentOrder(
        'IFDOCO',
          '1',
          'GTC',
          [$pop1->get_array(),$pop2->get_array(),$pop3->get_array()]
    );

    echo '<pre>';
    var_dump($client->send_childorder($co));
//    var_dump($client->send_parentorder($po));
    var_dump($client->get_parentorders());
    echo '</pre>';
    ?>
</body>
</html>


