<?php
/**
 * Created by IntelliJ IDEA.
 * User: kirin
 * Date: 2018/02/10
 * Time: 9:37
 */

namespace musikirin;

require_once 'GettableData.php';

class ParentOrder {
    use GettableData;

    private $order_method;
    private $minute_to_expire;
    private $time_in_force;
    private $parameters;

    function __construct($order_method, $minute_to_expire, $time_in_force, $parameters) {
        $this->order_method = $order_method ?: 'IFDOCO';
        $this->minute_to_expire = $minute_to_expire ?: '1';
        $this->time_in_force = $time_in_force ?: 'GTC';
        $this->parameters = $parameters ?: '';
    }
}

class ParentOrderParameter {
    use GettableData;

    private $product_code;
    private $condition_type;
    private $side;
    private $price;
    private $trigger_price;
    private $size;

    function __construct($product_code, $condition_type, $side, $price, $trigger_price, $size) {
        $this->product_code = $product_code ?: 'BTC_JPY';
        $this->condition_type = $condition_type ?: 'LIMIT';
        $this->side = $side ?: 'BUY';
        $this->price = $price ?: '100';
        $this->trigger_price = $trigger_price ?: '100';
        $this->size = $size ?: '0.01';
    }
}