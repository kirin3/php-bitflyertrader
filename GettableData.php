<?php
/**
 * Created by IntelliJ IDEA.
 * User: musikirin
 * Date: 2018/02/16
 * Time: 19:57
 */

namespace musikirin;

trait GettableData {
    /**
     * @return array
     */
    public function get_array() {
        $array = [];
        foreach ($this as $key => $value) {
            $array[$key] = $value;
        }
        return $array;
    }

    /**
     * @return string
     */
    public function get_json() {
        return json_encode($this->get_array());
    }

    /**
     * @return string
     */
    public function get_http_query() {
        return http_build_query($this->get_array());
    }
}