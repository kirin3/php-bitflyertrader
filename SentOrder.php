<?php
/**
 * Created by IntelliJ IDEA.
 * User: kirin
 * Date: 2018/02/10
 * Time: 9:37
 */

namespace musikirin;

require_once 'GettableData.php';

class SentChildOrder {
    use GettableData;

    private $product_code;
    private $child_order_id;
    private $child_order_id;
    private $child_order_acceptance_id;

    function __construct($product_code, $child_order_type, $side, $price, $size, $minute_to_expire, $time_in_force) {
        $this->product_code = $product_code ?: 'BTC_JPY';
        $this->child_order_type = $child_order_type ?: 'LIMIT';
        $this->side = $side ?: 'BUY';
        $this->price = $price ?: '100';
        $this->size = $size ?: '0.01';
        $this->minute_to_expire = $minute_to_expire ?: '1';
        $this->time_in_force = $time_in_force ?: 'GTC';
    }
}

class SentParentOrder {
    use GettableData;


    private $parent_order_id;
}