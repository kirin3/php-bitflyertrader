<?php
/**
 * Created by IntelliJ IDEA.
 * User: musikirin
 * Date: 2018/02/08
 * Time: 9:53
 */

namespace musikirin;

require_once 'vendor/autoload.php';
require_once 'ChildOrder.php';
require_once 'ParentOrder.php';
require_once 'OptionData.php';

use GuzzleHttp\Client;

/**
 * Class bitFlyerClient
 * @author musikirin <ninakirin3@gmail.com>
 */
class bitFlyerClient {

    // bitFlyer Lightningのエンドポイント。常にここを起点にAPIリクエストを行う。
    const END_POINT = "https://api.bitflyer.jp";
    const API_VERSION = "/v1/";

    private $market;
    private $API_key;
    private $API_secret;
    private $client;

    /**
     * bitFlyerClient constructor.
     * @param string $market 使用するマーケットを与える
     * @param string $API_key
     * @param string $API_secret
     */
    function __construct($market, $API_key, $API_secret) {
        $this->market = $market;
        $this->API_key = $API_key;
        $this->API_secret = $API_secret;
        $this->client = new Client(['base_uri' => self::END_POINT . self::API_VERSION, 'timeout' => 10.0]);
    }

    /**
     * Private API にアクセスするためのContext生成関数
     * @param string $method
     * @param string $request
     * @param null   $body
     * @return array
     */
    public function get_request_header($method = 'GET', $request = '', $body = null) {
        $time = time();
        $text = $time . $method . self::API_VERSION . $request . $body;
        $sign = hash_hmac('sha256', $text, $this->API_secret);
        $header = [
            "ACCESS-KEY" => $this->API_key,
            "ACCESS-TIMESTAMP" => $time,
            "ACCESS-SIGN" => $sign,
            "Content-Type" => "application/json",
        ];
        return $header;
    }

    /**
     * マーケット情報を取得する。
     * @return array
     */
    public function get_markets() {
        $response = $this->client->get("getmarkets");
        return json_decode($response->getBody(), true);
    }

    /**
     * 板情報を配列で取得。
     * @return array
     */
    public function get_board() {
        $response = $this->client->get("getboard?product_code=" . $this->market);
        return json_decode($response->getBody(), true);
    }

    /**
     * Tickerの一覧を取得。
     * @return array
     */
    public function get_ticker() {
        $response = $this->client->get("getticker?product_code=" . $this->market);
        return json_decode($response->getBody(), true);
    }

    /**
     * 約定履歴の一覧を取得。
     * @param \musikirin\OptionData|null $OptionData
     * @return array
     */
    public function get_executions(OptionData $OptionData = null) {
        $option = $OptionData ? $OptionData->get_uri('&') : '';
        $response = $this->client->get("getexecutions?product_code=" . $this->market . $option);
        return json_decode($response->getBody(), true);
    }

    /**
     * 板の状態を取得。
     * @return array
     */
    public function get_boardstate() {
        $response = $this->client->get("getboardstate?product_code=" . $this->market);
        return json_decode($response->getBody(), true);
    }

    /**
     * 取引所の状態を取得。
     * @return array
     */
    public function get_health() {
        $response = $this->client->get("gethealth?product_code=" . $this->market);
        return json_decode($response->getBody(), true);
    }

    /**
     * 取引所の状態を取得するためのURIを返す。
     * @param \musikirin\OptionData|null $OptionData
     * @return string
     */
    public function get_chats_uri(OptionData $OptionData = null) {
        $option = $OptionData ? $OptionData->get_uri('?') : '';
        $url = self::END_POINT . self::API_VERSION . "getchats" . $option;
        return $url;
    }

    /**
     * 取引所の状態を取得。
     * @param null $from_date
     * @return array
     */
    public function get_chats($from_date = null) {
        $option = $from_date ? '?from_date=' . $from_date : '';
        $response = $this->client->get("getchats" . $option);
        return json_decode($response->getBody(), true);
    }

    /**
     * 取引所の状態を取得。
     * @return array
     */
    public function get_permissions() {
        $response = $this->client->get(
            'me/getpermissions',
            ['headers' => $this->get_request_header('GET', 'me/getpermissions')]
        );
        return json_decode($response->getBody(), true);
    }

    /**
     * 資産残高を取得。
     * @return array
     */
    public function get_balance() {
        $response = $this->client->get(
            'me/getbalance',
            ['headers' => $this->get_request_header('GET', 'me/getbalance')]
        );
        return json_decode($response->getBody(), true);
    }

    /**
     * 証拠金の状態を取得。
     * @return array
     */
    public function get_collateral() {
        $response = $this->client->get(
            'me/getcollateral',
            ['headers' => $this->get_request_header('GET', 'me/getcollateral')]
        );
        return json_decode($response->getBody(), true);
    }

    /**
     * 各通貨の証拠金の状態を取得。
     * @return array
     */
    public function get_collateralaccounts() {
        $response = $this->client->get(
            'me/getcollateralaccounts',
            ['headers' => $this->get_request_header('GET', 'me/getcollateralaccounts')]
        );
        return json_decode($response->getBody(), true);
    }

    /**
     * 預入用アドレスを取得。
     * @return array
     */
    public function get_addresses() {
        $response = $this->client->get(
            'me/getaddresses',
            ['headers' => $this->get_request_header('GET', 'me/getaddresses')]
        );
        return json_decode($response->getBody(), true);
    }

    /**
     * 仮想通貨預入履歴を取得。
     * @param \musikirin\OptionData|null $OptionData
     * @return array
     */
    public function get_coinins(OptionData $OptionData = null) {
        $option = $OptionData ? $OptionData->get_uri('?') : '';
        $response = $this->client->get(
            'me/getcoinins' . $option,
            ['headers' => $this->get_request_header('GET', 'me/getcoinins' . $option)]
        );
        return json_decode($response->getBody(), true);
    }

    /**
     * 仮想通貨送付履歴を取得。
     * @param \musikirin\OptionData|null $OptionData
     * @return array
     */
    public function get_coinouts(OptionData $OptionData = null) {
        $option = $OptionData ? $OptionData->get_uri('?') : '';
        $response = $this->client->get(
            'me/getcoinouts' . $option,
            ['headers' => $this->get_request_header('GET', 'me/getcoinouts' . $option)]
        );
        return json_decode($response->getBody(), true);
    }

    /**
     * 銀行口座一覧を取得。
     * @return array
     */
    public function get_bankaccounts() {
        $res = $this->client->get(
            'me/getbankaccounts?product_code=' . $this->market,
            ['headers' => $this->get_request_header('GET', 'me/getbankaccounts?product_code='.$this->market)]
        );
        return json_decode($res->getBody(), true);
    }

    /**
     * 入金履歴を取得。
     * @param \musikirin\OptionData $OptionData
     * @return array
     */
    public function get_deposits(OptionData $OptionData = null) {
        $option = $OptionData ? $OptionData->get_uri('?') : '';
        $response = $this->client->get(
            'me/getdeposits' . $option,
            ['headers' => $this->get_request_header('GET', 'me/getdeposits' . $option)]
        );
        return json_decode($response->getBody(), true);
    }

    /**
     * 出金履歴を取得。
     * @param \musikirin\OptionData|null $OptionData
     * @return array
     */
    public function get_withdrawals(OptionData $OptionData = null) {
        $option = $OptionData ? $OptionData->get_uri('?') : '';
        $response = $this->client->get(
            'me/getwithdrawals' . $option,
            ['headers' => $this->get_request_header('GET', 'me/getwithdrawals' . $option)]
        );
        return json_decode($response->getBody(), true);
    }

    /**
     * 建玉の一覧を取得。
     * @return array
     */
    public function get_positions() {
        $res = $this->client->get(
            'me/getpositions?product_code=' . $this->market,
            ['headers' => $this->get_request_header('GET', 'me/getpositions?product_code=' . $this->market)]
        );
        return json_decode($res->getBody(), true);
    }

    /**
     * 証拠金の変動履歴を取得。
     * @param \musikirin\OptionData|null $OptionData
     * @return array
     */
    public function get_collateralhistory(OptionData $OptionData = null) {
        $option = $OptionData ? $OptionData->get_uri('?') : '';
        $response = $this->client->get(
            'me/getcollateralhistory' . $option,
            ['headers' => $this->get_request_header('GET', 'me/getcollateralhistory' . $option)]
        );
        return json_decode($response->getBody(), true);
    }

    /**
     * 取引手数料を取得。
     * @return array
     */
    public function get_tradingcommission() {
        $response = $this->client->get(
            'me/gettradingcommission?product_code=' . $this->market,
            [
                'headers' => $this->get_request_header(
                    'GET',
                    'me/gettradingcommission?product_code=' . $this->market
                )
            ]
        );
        return json_decode($response->getBody(), true);
    }

    /**
     * 小注文の一覧を取得。
     * @param \musikirin\OptionData|null $OptionData
     * @return array
     */
    public function get_childorders(OptionData $OptionData = null) {
        $option = $OptionData ? $OptionData->get_uri('?') : '';
        $response = $this->client->get(
            'me/getchildorders' . $option,
            ['headers' => $this->get_request_header('GET', 'me/getchildorders' . $option)]
        );
        return json_decode($response->getBody(), true);
    }

    /**
     * 子注文を作成。
     * @param \musikirin\ChildOrder $childOrder
     * @return array
     */
    public function send_childorder(ChildOrder $childOrder) {
        $response = $this->client->post(
            'me/sendchildorder',
            [
                'headers' => $this->get_request_header(
                    'POST',
                    'me/sendchildorder',
                    $childOrder->get_json()
                ),
                'body'    => $childOrder->get_json()
            ]
        );
        return json_decode($response->getBody(), true);
    }

    /**
     * 親注文の一覧を取得。
     * @param \musikirin\OptionData|null $OptionData
     * @return array
     */
    public function get_parentorders(OptionData $OptionData = null) {
        $option = $OptionData ? $OptionData->get_uri('?') : '';
        $response = $this->client->get(
            'me/getparentorders' . $option,
            ['headers' => $this->get_request_header('GET', 'me/getparentorders' . $option)]
        );
        return json_decode($response->getBody(), true);
    }

    /**
     * 親注文を作成。
     * @param ParentOrder $parentOrder
     * @return array
     */
    public function send_parentorder(ParentOrder $parentOrder) {
        $response = $this->client->post(
            'me/sendparentorder',
            [
                'headers' => $this->get_request_header(
                    'POST',
                    'me/sendparentorder',
                    $parentOrder->get_json()
                ),
                'body'    => $parentOrder->get_json()
            ]
        );
        return json_decode($response->getBody(), true);
    }


    /**
     * すべての注文をキャンセル
     * @return mixed
     */
    public function cancel_allchildorders(){
        $order = ["product_code" => $this->market];
        $response = $this->client->post(
            'me/cancelallchildorders',
            [
                'headers' => $this->get_request_header(
                    'POST',
                    'me/cancelallchildorders',
                    json_encode($order)
                ),
                'body'    => json_encode($order)
            ]
        );
        return $response->getStatusCode();
    }
}