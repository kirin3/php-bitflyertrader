<?php
/**
 * Created by IntelliJ IDEA.
 * User: kirin
 * Date: 2018/02/10
 * Time: 9:37
 */

namespace musikirin;

class OptionData {
    private $rawData;

    function __construct($array = []) {
        $this->rawData = $array;
    }

    /**
     * @param string $prefix
     * @return string
     */
    public function get_uri($prefix = '') {
        $uri = '';
        foreach ($this->rawData as $key => $value) {
            $uri = $uri . "$key=$value";
        }
        return $uri ? $prefix . $uri : '';
    }

    /**
     * @return $this
     */
    public function init() {
        $this->rawData = [];
        return $this;
    }

    /**
     * @return array
     */
    public function get_array() {
        return $this->rawData;
    }
}